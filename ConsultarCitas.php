<?php session_start(); ?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html lang="zxx">

<head>
    <title>Consultar Servicios</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Natural Spa Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <!-- gallery -->
    <link rel="stylesheet" href="css/smoothbox.css" type='text/css' media="all" />
    <!-- fontawesome -->
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <!-- online fonts -->
    <link href="//fonts.googleapis.com/css?family=Philosopher:400,400i,700,700i" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Markazi+Text:400,500,600,700" rel="stylesheet">

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <?php include 'obtenerCitas.php'; ?>
    <div id="home">
        <!-- inner banner -->
        <div class="inner-banner">
            <!-- header -->
            
            <nav class="navbar fixed-top navbar-expand-lg navbar-light navbar-fixed-top">
            <div class="container">
                <h1>
                    <a class="navbar-brand text-white" href="index.html">
                        NS
                    </a>
                </h1>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-lg-auto text-center align-items-center">
                        <li class="nav-item active  mr-lg-3 mt-lg-0 mt-3">
                            <a class="nav-link" href="index.html">Inicio
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item  mr-lg-3 mt-lg-0 mt-3">
                            <a class="nav-link" href="consultarCitas.php">Citas</a>
                        </li>
<!--                         <li class="nav-item dropdown mr-lg-3 mt-lg-0 mt-3">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                Dropdown
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="services.html">Services</a>
                                <a class="dropdown-item" href="gallery.html">Gallery</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="single.html">single page</a>

                            </div>
                        </li> -->
                        <li class="nav-item mr-lg-3 mt-lg-0 mt-3">
                            <a class="nav-link" href="registrarCita.php">Registar Cita</a>
                        </li>
                        <?php 
                            if(isset($_SESSION["usuario_id"])){
                        ?>
                            <li class="nav-item  mr-lg-3 mt-lg-0 mt-3">
                            <a class="nav-link" href="#">Bienvenido, <?php echo $_SESSION["usuario_nombre"]; ?></a>
                            </li>
                        <?php
                            } else {
                        ?>
                            <li class="nav-item  mr-lg-3 mt-lg-0 mt-3">
                            <a class="nav-link" href="login.php">Iniciar Sesión</a>
                        </li>
                        <?php
                            }
                        ?>
                        <li class="nav-item  position-relative">
                            <a href="#menu" id="toggle">
                                <span></span>
                            </a>
                            <div id="menu">
                                <ul>

                                    <li class="nav-item">
                                        <a href="register.php" class="nav-link text-dark">Registrarse</a>
                                    </li>
                                    <?php 
                                        if(isset($_SESSION["usuario_id"])){
                                    ?>
                                    <li class="nav-item">
                                        <a href="cerrarSesion.php" class="nav-link text-dark">Cerrar Sesión</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- /.container -->
        </nav>
            <!-- //header -->
        </div>
        <!-- //inner banner -->
        <!-- breadcrumbs -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">Inicio</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Citas</li>
            </ol>
        </nav>
        <!-- //breadcrumbs -->
        <!-- banner bottom -->
        <section class="w3ls-bnrbtm pt-sm-5 pt-3" id="w3layouts_bnrbtm">
            <div class="container pt-md-5">
                <div class="row pt-lg-5">
                    <div class="col-lg-5  bb-left">
                        <h3 class="agile-title">Citas
                            <span>SPA</span>
                            <br> Consultar citas
                            <span class="title-pos">Citas</span>
                        </h3>
                    </div>
                    <div class="col-lg-7 mt-lg-0 mt-3 px-lg-5">
                        <?php
                        if(isset($_SESSION['usuario_id'])){ 
                        ?>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Servicio</th>
                                    <th>Fecha y hora</th>
                                    <th>Descripción</th>
                                    <th>Cliente</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if($datos->num_rows >0){
                                    while ($row = $datos->fetch_assoc()) {
                                        echo "<tr>";
                                            echo "<td>".$row["id"]."</td>";
                                            echo "<td>".$row["servicio"]."</td>";
                                            echo "<td>".$row["fecha_hora"]."</td>";
                                            echo "<td>".$row["descripcion"]."</td>";
                                            echo "<td>".$_SESSION["usuario_nombre"]."</td>";
                                        echo "</tr>";
                                    } 
                                } else {
                                        echo "<tr>";
                                            echo "<td>No existen citas registradas para este cliente</td>";
                                        echo "</tr>";
                                    }
                                 ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="alert alert-danger" role="alert">
                          No tienes permisos para ver esta sección, favor de iniciar sesión.
                          <a href="login.php">Iniciar Sesión</a>
                        </div>
                    <?php } ?>
                    </div>
                </div>
                <div class="text-center">
                    <img src="images/about.png" alt="about" class="img-fluid mx-auto" />
                </div>
            </div>
        </section>
        <!-- //banner bottom -->
        <!-- footer -->
        <footer class="py-5 position-relative">
            <div class="container">
                <div class="footer-bottom row">
                    <!-- footer grid1 -->
                    <div class="col-lg-4 col-sm-6 footv3-left">
                        <!-- fixed social icons -->
                        <div id="fixed-social">
                            <h3 class="mb-4">Follow us on</h3>
                            <div>
                                <a href="#" class="fixed-facebook mb-2 rounded" target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                    <span class="rounded-left">Facebook</span>
                                </a>
                            </div>
                            <div>
                                <a href="#" class="fixed-twitter mb-2 rounded" target="_blank">
                                    <i class="fab fa-twitter"></i>
                                    <span class="rounded-left">Twitter</span>
                                </a>
                            </div>
                            <div>
                                <a href="#" class="fixed-gplus mb-2 rounded" target="_blank">
                                    <i class="fab fa-google-plus-g"></i>
                                    <span class="rounded-left">Google+</span>
                                </a>
                            </div>
                            <div>
                                <a href="#" class="fixed-linkedin mb-2 rounded" target="_blank">
                                    <i class="fab fa-linkedin-in"></i>
                                    <span class="rounded-left">LinkedIn</span>
                                </a>
                            </div>
                        </div>
                        <!-- //fixed social icons -->

                    </div>
                    <!-- //footer grid1 -->
                    <!-- footer grid3 -->
                    <div class="col-lg-4  col-sm-6 footv3-left my-sm-0 my-4">
                        <h3 class="mb-4">Links</h3>
                        <ul class="list-agileits">
                            <li>
                                <a href="index.html">
                                    Home
                                </a>
                            </li>
                            <li class="my-3">
                                <a href="about.html">
                                    About Us
                                </a>
                            </li>
                            <li class="mb-3">
                                <a href="services.html">
                                    Services
                                </a>
                            </li>
                            <li>
                                <a href="contact.html">
                                    Contact Us
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- //footer grid3 -->
                    <!-- footer grid4  -->
                    <div class="col-lg-4  footv3-left my-lg-0 mt-4">
                        <h3 class="mb-4">subscribe to newsletter</h3>
                        <form action="#" method="post">
                            <div class="form-group">
                                <input type="email" class="form-control  bg-dark border-0" id="email" placeholder="Enter email" name="email" required>
                            </div>
                            <div class="form-group">
                                <input type="Submit" class="form-control  border-0" id="sub" value="submit" name="sub">
                            </div>
                        </form>
                    </div>
                    <!-- //footer grid4 -->
                </div>
            </div>
            <!-- //footer container -->
        </footer>
        <!-- //footer -->
        <div class="cpy-right text-center  py-3 bg-dark">
            <p class="text-white">© 2018 Natural Spa. All rights reserved | Design by
                <a href="http://w3layouts.com" class="text-white"> W3layouts.</a>
            </p>
        </div>
    </div>
    <!-- js -->
    <script src="js/jquery-2.2.3.min.js"></script>
    <!-- dropdown nav -->
    <script>
        $(document).ready(function () {
            $(".dropdown").hover(
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                    $(this).toggleClass('open');
                },
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                    $(this).toggleClass('open');
                }
            );
        });
    </script>
    <!--  menu toggle -->
    <script src="js/menu.js"></script>
    <!-- gallery  -->
    <script src="js/smoothbox.jquery2.js"></script>
    <!-- Scrolling Nav JavaScript -->
    <script src="js/scrolling-nav.js"></script>
    <!-- team-->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <script src="js/owl.carousel.js"></script>
    <script>
        $(document).ready(function () {
            $("#owl-demo").owlCarousel({
                items: 1,
                lazyLoad: true,
                autoPlay: false,
                navigation: true,
                navigationText: true,
                pagination: true,
            });
        });
    </script>
    <!-- // team -->
    <!--  testimonials Responsiveslides -->
    <script src="js/responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider3").responsiveSlides({
                auto: false,
                pager: true,
                nav: false,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <!--  testimonials Responsiveslides -->
    <!-- counter -->
    <script src="js/counter.js">
    </script>
    <!-- smooth scroll -->
    <script src="js/SmoothScroll.min.js"></script>
    <!--/ start-smoth-scrolling -->
    <script src="js/move-top.js"></script>
    <script src="js/easing.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 900);
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            /*
            						var defaults = {
            							  containerID: 'toTop', // fading element id
            							containerHoverID: 'toTopHover', // fading element hover id
            							scrollSpeed: 1200,
            							easingType: 'linear' 
            						 };
            						*/

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <!--// end-smoth-scrolling -->
    <script src="js/bootstrap.js"></script>
</body>

</html>